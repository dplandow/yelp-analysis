This repository includes a Java program that analyses Yelp reviews for 85,000+ restaurants. The program uses the term frequency-inverse document frequency of each word to determine which words accuratly represent each business.

The output includes the business ID, business name, business address, number of characters in the business's reveiws, and the top 30 words for the 10 restaurants with the most reviews.

The Yelp data set is also provided in the repository.