//package hw6;
import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Collections;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import java.io.IOException;
import java.lang.NullPointerException;
import java.lang.Double;
import java.lang.System;

public class YelpAnalysis {
	public static void main (String[] args) {
		try {
			BufferedReader file_reader = new BufferedReader(new FileReader("yelpDatasetParsed_full.txt"));

			Map<String, Integer> corpusDFCount = new HashMap<>();

			PriorityQueue<Business> businessQueue = new PriorityQueue<>(new Business_compare());
			while (true) {
				try {
					Business b = readBusiness(file_reader);
					addDocumentCount(corpusDFCount, b);
					businessQueue.add(b);
					if (businessQueue.size() > 10)
						businessQueue.remove();
				}
				catch (NullPointerException ex) {
					break;
				}
			}
			// for the top 10 businesses with most review characters
			for (int i = 0; i < 10; i++) {
				Business currB = businessQueue.poll();
				Map<String, Double> tfidfScoreMap = getTfidfScore(corpusDFCount, currB, 5);
				// Entry is a static nested interface of class Map
				List<Map.Entry<String, Double>> tfidfScoreList = new ArrayList <>(tfidfScoreMap.entrySet());
				sortByTfidf(tfidfScoreList);
				System.out.println(currB);
				printTopWords(tfidfScoreList, 30);
			}

			file_reader.close();
		}
		catch (IOException ex) {
			System.out.println("File not found");
		}
	}

	private static Business readBusiness(BufferedReader file_reader) {
		try {
			String whole_line = file_reader.readLine();
			String business_info = whole_line.substring(1, whole_line.length() - 1);
			String[] info_split = business_info.split(", ");
	  		String businessID = info_split[0];
	 		String businessName = info_split[1];
	  		String businessAddress = info_split[2];
	  		String reviews = info_split[3];
	  		int reviewCharCount = reviews.length();

	  		return new Business(businessID, businessName, businessAddress, reviews, reviewCharCount);
	  	}
	  	catch (IOException ex) {
	  		return null;
	  	}
	}

	private static void addDocumentCount(Map<String, Integer> corpusDFCount, Business b) {
		Set<String> words_b = new HashSet<>();
		Scanner word_finder = new Scanner(b.reviews);
		String next_word;
		while(word_finder.hasNext() == true) {
			next_word = word_finder.next();
			if (words_b.contains(next_word)) {}
			else if (corpusDFCount.containsKey(next_word)) {
				words_b.add(next_word);
				corpusDFCount.replace(next_word, corpusDFCount.get(next_word) + 1);
			}
			else {
				words_b.add(next_word);
				corpusDFCount.put(next_word, 1);
			}
		}
		word_finder.close();
	}

	public static class Business_compare implements Comparator<Business> {
		public int compare(Business business1, Business business2) {
		    if (business1.reviewCharCount < business2.reviewCharCount)
		      	return -1;
		    else if (business1.reviewCharCount > business2.reviewCharCount)
		      	return 1;
		    else
		      	return 0;
		  }
	}

	private static Map<String, Double> getTfidfScore(Map<String, Integer> corpusDFCount, Business currB, int min) {
		Scanner word_finder = new Scanner(currB.reviews);
		Map<String, Integer> word_count = new HashMap<>();
		String next_word;
		while (word_finder.hasNext() == true) {
			next_word = word_finder.next();
			if (word_count.containsKey(next_word))
				word_count.put(next_word, word_count.get(next_word) + 1);
			else
				word_count.put(next_word, 1);
		}

		Map<String, Double> tfidf_scores = new HashMap<>();
		Double tfidf_score;
		for (Map.Entry<String, Integer> entry : word_count.entrySet()) {
			if (corpusDFCount.get(entry.getKey()) < min)
				tfidf_score = 0.0;
			else
				tfidf_score = (double)entry.getValue() / corpusDFCount.get(entry.getKey());
			tfidf_scores.put(entry.getKey(), tfidf_score);
		}

		word_finder.close();

		return tfidf_scores;
	}

	public static class Tfidf_compare implements Comparator<Map.Entry<String, Double>> {
		public int compare(Map.Entry<String, Double> entry1, Map.Entry<String, Double> entry2) {
			if (entry1.getValue() < entry2.getValue())
				return 1;
			if (entry1.getValue() > entry2.getValue())
				return -1;
			else 
				return 0;
		}
	}

	private static void sortByTfidf(List<Map.Entry<String, Double>> tfidfScoreList) {
		Collections.sort(tfidfScoreList, new Tfidf_compare());
	}

	private static void printTopWords(List<Map.Entry<String, Double>> tfidfScoreList, int quantity) {
		Map.Entry<String, Double> current_entry;
		int i = 0;
		for (Iterator<Map.Entry<String, Double>> iter = tfidfScoreList.iterator(); i < quantity && iter.hasNext(); i++) {
			current_entry = iter.next();
			System.out.print("(" + current_entry.getKey() + "," + current_entry.getValue() + ") ");
		}
		System.out.print("\n");
	}
}